## Installing necessary dependencies

```
npm i serverless -g
```
then

```
npm i 
```

## Running the app locally

```
docker-compose up -d mysql
```

```
npm start
```

## Setting up database data

Go to: https://127.0.0.1:3000/setup

to loging use:
POST on https://127.0.0.1:3000/auth/login
with 
```
{
  username: testUser
  password: testPassword
}
```

## Diagram
![Diagram]
(/images/diagram.jpg)