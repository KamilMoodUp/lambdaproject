import * as moment from 'moment';

export function calculateExpirationDate(expirationTime: number): number {
  const now = moment(new Date(), moment.ISO_8601);
  return moment(now)
    .add(expirationTime)
    .unix();
}