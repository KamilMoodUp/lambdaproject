import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-http-bearer';

import { AccessTokenJSON } from '../../../auth/entities/accessToken.entity';
import { User } from '../../../auth/entities/user.entity';
import { AuthService } from '../../../auth/services/auth.service';

export const ACCESS_TOKEN_STRATEGY = 'accessToken';

@Injectable()
export class AccessTokenStrategy extends PassportStrategy(Strategy, ACCESS_TOKEN_STRATEGY) {
  constructor(
    private readonly authService: AuthService,
  ) {
    super();
  }

  async validate(token: string): Promise<User> {
    const accessToken: AccessTokenJSON= await this.authService.validateToken(token);
    if (!accessToken) {
      throw new UnauthorizedException();
    }

    return this.authService.findUserById(accessToken.userId);
  }
}
