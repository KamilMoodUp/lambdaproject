import { Body, Controller, ForbiddenException, Get, Param, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { User } from '../../auth/entities/user.entity';
import { CreatePpvEventDto } from '../../ppvEvent/dto/createPpvEvent.dto';
import { PpvEvent } from '../../ppvEvent/entities/ppvEvent.entity';
import { PpvEventService } from '../../ppvEvent/services/ppvEvent.service';
import { AuthorizedUser } from '../middlewares/decorators/authorizedUser.decorator';
import { ACCESS_TOKEN_STRATEGY } from '../middlewares/strategies/accessToken.strategy';

@Controller('ppvEvents')
export class PpvEventController {
  constructor(private readonly ppvEventService: PpvEventService) { }

  @Get('/:ppvEventId')
  @UseGuards(AuthGuard(ACCESS_TOKEN_STRATEGY))
  async getEvent(
    @AuthorizedUser() user: User,
    @Param('ppvEventId') ppvEventId: string
  ): Promise<PpvEvent> {
    const ppvEvent = await this.ppvEventService.getPpvEvent(ppvEventId);
    if (!user.viewer.haveAccessToEvent(ppvEvent)) {
      throw new ForbiddenException();
    }
    return ppvEvent;
  }

  @Post()
  async createEvent(
    @Body() body: CreatePpvEventDto
  ): Promise<PpvEvent> {
    return this.ppvEventService.createPpvEvent(body);
  }
}
