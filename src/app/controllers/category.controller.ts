import { Body, Controller, Get, Post } from '@nestjs/common';

import { CreateCategoryDto } from '../../category/dto/createCategory.dto';
import { CategoryService } from '../../category/services/category.service';

@Controller('categories')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) { }

  @Get()
  getCategories() {
    return this.categoryService.getCategories();
  }

  @Post()
  createCategory(
    @Body() body: CreateCategoryDto
  ) {
    return this.categoryService.createCategory(body);
  }
}
