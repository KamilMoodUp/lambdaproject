import { Body, Controller, Get, Post } from '@nestjs/common';

import { CreateSubscriptionPlanDto } from '../../subscriptionPlan/dto/createSubscriptionPlan.dto';
import { SubscriptionPlanService } from '../../subscriptionPlan/services/subscriptionPlan.service';

@Controller('subscriptionPlans')
export class SubscriptionPlanController {
  constructor(private readonly subscriptionPlanService: SubscriptionPlanService) { }
  
  @Get()
  getSubscriptionPlans() {
    
  }

  @Post()
  createSubscriptionPlan(@Body() body: CreateSubscriptionPlanDto) {
    return this.subscriptionPlanService.createSubscriptionPlan(body);
  }
}
