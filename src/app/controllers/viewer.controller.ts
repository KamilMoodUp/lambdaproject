import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { User } from '../../auth/entities/user.entity';
import { GrantEntitlementDto } from '../../viewer/dto/grantEntitlement.dto';
import { RevoketEntitlementDto } from '../../viewer/dto/revokeEntitlement.dto';
import { IViewer } from '../../viewer/interfaces/viewer.interface';
import { ViewerService } from '../../viewer/services/viewer.service';
import { AuthorizedUser } from '../middlewares/decorators/authorizedUser.decorator';
import { ACCESS_TOKEN_STRATEGY } from '../middlewares/strategies/accessToken.strategy';

@Controller('viewers')
export class ViewerController {
  constructor(private readonly viewerService: ViewerService) { }

  @Get('/self')
  @UseGuards(AuthGuard(ACCESS_TOKEN_STRATEGY))
  getSelf(
    @AuthorizedUser() user: User,
  ): IViewer {
    return user.viewer;
  }

  @Post('/grant-entitlement')
  @UseGuards(AuthGuard(ACCESS_TOKEN_STRATEGY))
  grantEntitlement(
    @AuthorizedUser() user: User,
    @Body() body: GrantEntitlementDto
  ): Promise<IViewer> {
    return this.viewerService.grantEntitlement(user.viewer.id, body);
  }

  @Post('/revoke-entitlement')
  @UseGuards(AuthGuard(ACCESS_TOKEN_STRATEGY))
  revokeEntitlement(
    @AuthorizedUser() user: User,
    @Body() body: RevoketEntitlementDto
  ): Promise<IViewer> {
    return this.viewerService.revokeEntitlement(user.viewer.id, body);
  }
}
