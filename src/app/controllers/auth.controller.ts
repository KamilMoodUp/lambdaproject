import { Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { AuthService } from '../../auth/services/auth.service';
import { AuthorizedUser } from '../middlewares/decorators/authorizedUser.decorator';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @Post('/login')
  @UseGuards(AuthGuard('local'))
  async login(@AuthorizedUser() user) {
    return this.authService.login(user);
  }
}
