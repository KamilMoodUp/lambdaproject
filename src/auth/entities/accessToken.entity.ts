import { BaseEntity, BeforeInsert, Column, Entity, Generated, Index, PrimaryGeneratedColumn } from 'typeorm';

import { calculateExpirationDate } from '../../app/utils/calculateExpirationDate';
import { IAccessToken } from '../interfaces/accessToken.interface';

export const TOKEN_EXPIRATION_TIME = 9999999999;

export interface AccessTokenJSON {
  value: string,
  expires: number,
  userId: string
}

@Entity()
export class AccessToken extends BaseEntity implements IAccessToken {

  public constructor(data?: Partial<AccessToken>) {
    super();
    if (data) {
      Object.assign(this, data);
    }
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Index()
  @Column()
  @Generated('uuid')
  value: string;

  @Column()
  userId: string;

  @Column()
  expires: number;

  @BeforeInsert()
  setExpirationDate() {
    this.expires = calculateExpirationDate(TOKEN_EXPIRATION_TIME);
  }

  toJSON(): AccessTokenJSON {
    return {
      value: this.value,
      expires: this.expires,
      userId: this.userId
    };
  }
}