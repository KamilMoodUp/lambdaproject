import { compare, genSaltSync, hashSync } from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  getRepository,
  Index,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Viewer } from '../../viewer/entities/viewer.entity';
import { IUser } from '../interfaces/user.interface';
import { AccessToken, TOKEN_EXPIRATION_TIME } from './accessToken.entity';

export interface TokenData {
  token: string;
  expires: number;
}

export const SECRET = 'secret';

@Entity()
export class User extends BaseEntity implements IUser {
  constructor() {
    super();
  }
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  @Index({ unique: true })
  username: string;

  @Column({
    select: false,
  })
  password: string;

  @OneToOne(type => Viewer, {
    cascade: true,
    eager: true,
  })
  @JoinColumn()
  viewer: Viewer

  @BeforeInsert()
  @BeforeUpdate()
  hashPassword() {
    if (this.password) {
      this.password = this.generateHash(this.password);
    }
  }

  async login(): Promise<TokenData> {
    const accessToken = new AccessToken({ userId: this.id });
    await accessToken.save();
    const tokenData =
    {
      token: jwt.sign(accessToken.toJSON(), SECRET, { expiresIn: TOKEN_EXPIRATION_TIME }),
      expires: accessToken.expires
    }
    return tokenData;
  }

  async verifyPassword(passwordToVerify: string) {
    const repository = getRepository(User);
    const result = await repository.createQueryBuilder()
      .select(['password'])
      .where({
        id: this.id
      }).execute();
    return compare(passwordToVerify, result[0].password);
  }

  generateHash(password: string) {
    const salt = genSaltSync(10);
    const hashedPassword = hashSync(password, salt);
    return hashedPassword;
  }
}