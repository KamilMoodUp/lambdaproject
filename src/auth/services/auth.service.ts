import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as jwt from 'jsonwebtoken';
import { DeepPartial, FindOneOptions, Repository } from 'typeorm';

import { AccessTokenJSON } from '../entities/accessToken.entity';
import { SECRET, TokenData, User } from '../entities/user.entity';


@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
  ) { }

  validateToken(token: string): AccessTokenJSON {
    try {
      const verifiedToken = jwt.verify(token, SECRET);
      return verifiedToken;
    } catch (error) {
      return null;
    }
  }

  async authorizeUser(username: string, password: string) {
    const where: FindOneOptions<User>['where'] = {
      username,
    };
    const user = await this.userRepo.findOne(where);
    if (!user) {
      throw new UnauthorizedException('You are not authorized for this content');
    }

    const passwordVerified = await user.verifyPassword(password);
    if (!passwordVerified) {
      return null;
    }
    return user;
  }

  async login(user: User): Promise<TokenData> {
    return user.login();
  }

  async createUser(data: DeepPartial<User>): Promise<User> {
    return this.userRepo.save(data);
  }

  async findUserById(userId: string): Promise<User> {
    return this.userRepo.findOne(userId);
  }
}
