import { IViewer } from '../../viewer/interfaces/viewer.interface';

export interface IUser {
  id: string,
  username: string,
  password: string,
  viewer: IViewer
}