export interface IAccessToken {
  id: number,
  value: string,
  userId: string,
  expires: number,
}