import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ViewerController } from '../../app/controllers/viewer.controller';
import { Entitlement } from '../../viewer/entities/entitlement.entity';
import { Viewer } from '../../viewer/entities/viewer.entity';
import { ViewerService } from '../../viewer/services/viewer.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Viewer, Entitlement])
  ],
  controllers: [ViewerController],
  providers: [ViewerService],
})
export class ViewerModule { }
