import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { SubscriptionPlanController } from '../../app/controllers/subscriptionPlan.controller';
import { SubscriptionPlan } from '../../subscriptionPlan/entities/subscriptionPlan.entity';
import { SubscriptionPlanService } from '../../subscriptionPlan/services/subscriptionPlan.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([SubscriptionPlan])
  ],
  controllers: [SubscriptionPlanController],
  providers: [SubscriptionPlanService],
})
export class SubscriptionPlanModule { }
