import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { PpvEventController } from '../../app/controllers/ppvEvent.controller';
import { PpvEvent } from '../../ppvEvent/entities/ppvEvent.entity';
import { PpvEventService } from '../../ppvEvent/services/ppvEvent.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([PpvEvent])
  ],
  controllers: [PpvEventController],
  providers: [PpvEventService],
})
export class PpvEventModule { }
