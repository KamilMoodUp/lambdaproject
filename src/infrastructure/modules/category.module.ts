import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CategoryController } from '../../app/controllers/category.controller';
import { Category } from '../../category/entities/category.entity';
import { CategoryService } from '../../category/services/category.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Category])
  ],
  controllers: [CategoryController],
  providers: [CategoryService],
})
export class CategoryModule { }
