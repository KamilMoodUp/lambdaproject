import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthController } from '../../app/controllers/auth.controller';
import { AccessTokenStrategy } from '../../app/middlewares/strategies/accessToken.strategy';
import { LocalStrategy } from '../../app/middlewares/strategies/local.strategy';
import { User } from '../../auth/entities/user.entity';
import { AuthService } from '../../auth/services/auth.service';

@Module({
  imports: [
    PassportModule,
    TypeOrmModule.forFeature([User])
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    LocalStrategy,
    AccessTokenStrategy
  ],
})
export class AuthModule { }
