import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { User } from './auth/entities/user.entity';
import { Category } from './category/entities/category.entity';
import { PpvEvent } from './ppvEvent/entities/ppvEvent.entity';
import { SubscriptionPlan } from './subscriptionPlan/entities/subscriptionPlan.entity';
import { Entitlement } from './viewer/entities/entitlement.entity';
import { Viewer } from './viewer/entities/viewer.entity';
import { EntitlementType } from './viewer/interfaces/entitlement.interface';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    @InjectRepository(Viewer) private readonly viewerRepo: Repository<Viewer>,
    @InjectRepository(Category) private readonly categoryRepo: Repository<Category>,
    @InjectRepository(SubscriptionPlan) private readonly subscriptionPlanRepo: Repository<SubscriptionPlan>,
    @InjectRepository(PpvEvent) private readonly ppvEventRepo: Repository<PpvEvent>
  ) {}

  async setupDatabase(): Promise<Viewer> {
    const user = await this.userRepo.save(this.userRepo.create({ username: 'testUser', password: 'testPassword', viewer: {} }));
    const viewer = await this.viewerRepo.findOne(user.viewer.id);
    const categorySport = await this.categoryRepo.save({ name: 'sport' });
    const categoryBusines = await this.categoryRepo.save({ name: 'biznes' });
    const categoryArt = await this.categoryRepo.save({ name: 'art' });

    const subscriptionPlanSilver = await this.subscriptionPlanRepo.save({
      name: 'silver',
      categoryIds: [categoryArt.id],
    });
    const subscriptionPlanGold = await this.subscriptionPlanRepo.save({
      name: 'gold',
      categoryIds: [categorySport.id],
      lowerPlanId: subscriptionPlanSilver.id
    });
    await this.subscriptionPlanRepo.save({
      name: 'platine',
      categoryIds: [categoryBusines.id],
      lowerPlanId: subscriptionPlanGold.id
    });

    const entitlement = new Entitlement();
    entitlement.subscriptionPlan = subscriptionPlanGold;
    entitlement.type = EntitlementType.Subscription;
    await this.ppvEventRepo.save({ name: 'artEvent', categoryId: categoryArt.id })
    await this.ppvEventRepo.save({ name: 'businesEvent', categoryId: categoryBusines.id })
    return await this.viewerRepo.save({ ...viewer, entitlements: [entitlement] });
  }
}
