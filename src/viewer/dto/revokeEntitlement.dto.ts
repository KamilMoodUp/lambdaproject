import { IsUUID } from 'class-validator';

export class RevoketEntitlementDto {
  @IsUUID()
  entitlementId?: string;
}