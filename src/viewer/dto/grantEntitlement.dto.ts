import { IsEnum, IsUUID } from 'class-validator';

import { EntitlementType } from '../interfaces/entitlement.interface';

export class GrantEntitlementDto {
  @IsUUID()
  subscriptionPlanId?: string;

  @IsUUID()
  ppvEventId?: string;

  @IsEnum(EntitlementType)
  type: EntitlementType;
}