import { PpvEvent } from '../../ppvEvent/entities/ppvEvent.entity';
import { SubscriptionPlan } from '../../subscriptionPlan/entities/subscriptionPlan.entity';
import { Viewer } from '../entities/viewer.entity';

export enum EntitlementType {
  Subscription = 'subscription',
  SeasonPass = 'seasonPass',
  Event = 'event'
}

export interface BaseEntitlemet {
  id: string;
  viewer: Viewer;
  expires: number;
}

export interface SeasonPassEntitlement extends BaseEntitlemet {
  type: EntitlementType.SeasonPass;
}

export interface SubscriptionEntitlement extends BaseEntitlemet {
  type: EntitlementType.Subscription;
  subscriptionPlan: SubscriptionPlan;
}

export interface EventEntitlement extends BaseEntitlemet {
  type: EntitlementType.Event;
  ppvEvent: PpvEvent;
}

export interface IEntitlement extends BaseEntitlemet {
  type: EntitlementType;
  subscriptionPlan?: SubscriptionPlan;
  ppvEvent?: PpvEvent;
}

export type TEntitlement = SeasonPassEntitlement | SubscriptionEntitlement | EventEntitlement;