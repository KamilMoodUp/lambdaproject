import { User } from '../../auth/entities/user.entity';
import { Entitlement } from '../entities/entitlement.entity';

export interface IViewer {
  id: string;
  entitlements: Entitlement[]
  user: User;
}