import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PpvEvent } from '../../ppvEvent/entities/ppvEvent.entity';
import { SubscriptionPlan } from '../../subscriptionPlan/entities/subscriptionPlan.entity';
import { GrantEntitlementDto } from '../dto/grantEntitlement.dto';
import { RevoketEntitlementDto } from '../dto/revokeEntitlement.dto';
import { Entitlement } from '../entities/entitlement.entity';
import { Viewer } from '../entities/viewer.entity';


@Injectable()
export class ViewerService {
  constructor(
    @InjectRepository(Viewer) private readonly viewerRepo: Repository<Viewer>,
    @InjectRepository(SubscriptionPlan) private readonly subscriptionPlanRepo: Repository<SubscriptionPlan>,
    @InjectRepository(PpvEvent) private readonly ppvEventRepo: Repository<PpvEvent>,
  ) { }

  async getViewer(viewerId: string): Promise<Viewer> {
    return this.viewerRepo.findOne(viewerId);
  }

  async grantEntitlement(viewerId: string, data: GrantEntitlementDto): Promise<Viewer> {
    const viewer = await this.viewerRepo.findOne(viewerId);
    const entitlement = new Entitlement();
    entitlement.type = data.type;
    entitlement.viewer = viewer;

    if (data.subscriptionPlanId) {
      const subscriptionPlan = await this.subscriptionPlanRepo.findOne(data.subscriptionPlanId);
      if(!subscriptionPlan) throw new NotFoundException(`SubscriptionPlan with id ${data.subscriptionPlanId} does not exist.`)
      entitlement.subscriptionPlan = subscriptionPlan;
    }

    if (data.ppvEventId) {
      const ppvEvent = await this.ppvEventRepo.findOne(data.ppvEventId)
      if (!ppvEvent) throw new NotFoundException(`PpvEvent with id ${data.ppvEventId} does not exist.`)
      entitlement.ppvEvent = ppvEvent;
    }
    
    viewer.grantEntitlement(entitlement);
    return viewer;
  }


  async revokeEntitlement(viewerId: string, data: RevoketEntitlementDto): Promise<Viewer> {
    const viewer = await this.viewerRepo.findOne(viewerId);
    viewer.revokeEntitlement(data.entitlementId);
    return viewer;
  }
}
