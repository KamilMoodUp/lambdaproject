import * as moment from 'moment';
import { BaseEntity, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { User } from '../../auth/entities/user.entity';
import { PpvEvent } from '../../ppvEvent/entities/ppvEvent.entity';
import { EntitlementType, EventEntitlement } from '../interfaces/entitlement.interface';
import { IViewer } from '../interfaces/viewer.interface';
import { Entitlement } from './entitlement.entity';

@Entity()
export class Viewer extends BaseEntity implements IViewer {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToMany(type => Entitlement, entitlement => entitlement.viewer, { eager: true, cascade: true })
  @JoinColumn()
  entitlements: Entitlement[];

  @OneToOne(type => User)
  user: User;

  private hasSeasonPass(entitlements: Entitlement[]): boolean {
    return !!entitlements.find(entitlement => entitlement.type === EntitlementType.SeasonPass);
  }

  grantEntitlement(entitlement: Entitlement) {
    this.entitlements.push(entitlement);
    this.save();
  }

  revokeEntitlement(entitlementId) {
    const entitlements = this.entitlements.filter(entitlement => entitlement.id !== entitlementId);
    this.entitlements = entitlements;
    this.save();
  }

  haveAccessToEvent(event: PpvEvent): boolean {
    let haveAccess = false;
    const now = moment(new Date(), moment.ISO_8601).unix();
    const currentEntitlements = this.entitlements.filter(entitlement => entitlement.expires > now); 
    if (this.hasSeasonPass(currentEntitlements)) {
      return true;
    }

    currentEntitlements.map(entitlement => {
      switch (entitlement.type) {
        case EntitlementType.Event:
          if ((entitlement as EventEntitlement).ppvEvent.id === event.id) haveAccess = true;
          break;
        case EntitlementType.Subscription:
          if (entitlement.subscriptionPlan.categoryIds.find(categoryId => categoryId === event.categoryId)) haveAccess = true;
      }
    })
    return haveAccess;
  }
}