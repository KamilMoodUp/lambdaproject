import { BaseEntity, BeforeInsert, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { calculateExpirationDate } from '../../app/utils/calculateExpirationDate';
import { PpvEvent } from '../../ppvEvent/entities/ppvEvent.entity';
import { SubscriptionPlan } from '../../subscriptionPlan/entities/subscriptionPlan.entity';
import { EntitlementType, IEntitlement } from '../interfaces/entitlement.interface';
import { Viewer } from './viewer.entity';

const SUBSCRIPTION_EXPIRATION_TIME = 9999999999;

@Entity()
export class Entitlement extends BaseEntity implements IEntitlement {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Viewer, viewer => viewer.entitlements)
  viewer: Viewer;

  @ManyToOne(type => SubscriptionPlan, { eager: true })
  subscriptionPlan?: SubscriptionPlan;

  @ManyToOne(type => PpvEvent, { eager: true })
  ppvEvent?: PpvEvent;

  @Column()
  type: EntitlementType;

  @Column()
  expires: number;

  @BeforeInsert()
  setExpirationDate() {
    this.expires = calculateExpirationDate(SUBSCRIPTION_EXPIRATION_TIME);
  }
}