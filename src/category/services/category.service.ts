import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';

import { Category } from '../entities/category.entity';


@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category) private readonly categoryRepo: Repository<Category>,
  ) { }

  getCategories(): Promise<Category[]> {
    return this.categoryRepo.find();
  }

  createCategory(data: DeepPartial<Category>): Promise<Category> {
    return this.categoryRepo.save(data);
  }
}
