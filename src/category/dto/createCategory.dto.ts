import { MinLength } from 'class-validator';

export class CreateCategoryDto {
  @MinLength(3)
  name: string
}