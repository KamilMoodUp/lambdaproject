import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { ICategory } from '../interfaces/category.interface';

@Entity()
export class Category extends BaseEntity implements ICategory {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  
  @Column()
  name: string;
}