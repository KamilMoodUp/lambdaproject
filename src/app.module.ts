import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from 'nestjs-config';
import * as path from 'path';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmConfigService } from './infrastructure/database/database';
import { AuthModule } from './infrastructure/modules/auth.module';
import { CategoryModule } from './infrastructure/modules/category.module';
import { PpvEventModule } from './infrastructure/modules/ppvEvent.module';
import { SubscriptionPlanModule } from './infrastructure/modules/subscriptionPlan.module';
import { ViewerModule } from './infrastructure/modules/viewer.module';

@Global()
@Module({
  imports: [
    TypeOrmModule.forRootAsync(
      {
        inject: [ConfigModule],
        useClass: TypeOrmConfigService,
      }),
    ConfigModule.load(path.resolve(__dirname, 'config', '**', '!(*.d).{ts,js}')),
    ViewerModule,
    AuthModule,
    CategoryModule,
    PpvEventModule,
    SubscriptionPlanModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
