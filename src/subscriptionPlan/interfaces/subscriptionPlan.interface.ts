export interface ISubscriptionPlan {
  id: string,
  categoryIds: string[],
  lowerPlanId: string,
  name: string,
}