import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';

import { Category } from '../../category/entities/category.entity';
import { SubscriptionPlan } from '../entities/subscriptionPlan.entity';

@Injectable()
export class SubscriptionPlanService {
  constructor(
    @InjectRepository(SubscriptionPlan) private readonly subscriptionPlanRepo: Repository<SubscriptionPlan>,
    @InjectRepository(Category) private readonly categoryPlanRepo: Repository<SubscriptionPlan>,
  ) { }

  getSubscriptionPlans(): Promise<SubscriptionPlan[]> {
    return this.subscriptionPlanRepo.find();
  }

  createSubscriptionPlan(data: DeepPartial<SubscriptionPlan>): Promise<SubscriptionPlan> {
    return this.subscriptionPlanRepo.save(data);
  }
}
