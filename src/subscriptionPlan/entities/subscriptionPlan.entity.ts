import { AfterLoad, BaseEntity, Column, Entity, getRepository, PrimaryGeneratedColumn } from 'typeorm';

import { ISubscriptionPlan } from '../interfaces/subscriptionPlan.interface';

@Entity()
export class SubscriptionPlan extends BaseEntity implements ISubscriptionPlan {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('simple-array')
  categoryIds: string[];

  @Column({ nullable: true, default: null })
  lowerPlanId: string;

  @Column()
  name: string;

  @AfterLoad() 
  private async getCategoryIdsFromLowerPlans(): Promise<void> {
    if (this.lowerPlanId) {
      this.categoryIds = this.categoryIds.concat(await (await getRepository(SubscriptionPlan).findOne(this.lowerPlanId)).categoryIds);  
    }
  }
}