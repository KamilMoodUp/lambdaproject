import { IsArray, IsOptional, IsUUID, MinLength } from 'class-validator';

export class CreateSubscriptionPlanDto {
  @IsArray()
  categoryIds: string[];

  @IsOptional()
  @IsUUID()
  lowerPlanId?: string;

  @MinLength(3)
  name: string;
}