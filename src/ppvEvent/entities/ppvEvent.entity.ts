import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { IPpvEvent } from '../interfaces/ppvEvent.interface';

@Entity()
export class PpvEvent extends BaseEntity implements IPpvEvent {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  categoryId: string;
}