import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';

import { PpvEvent } from '../../ppvEvent/entities/ppvEvent.entity';


@Injectable()
export class PpvEventService {
  constructor(
    @InjectRepository(PpvEvent) private readonly ppvEventRepo: Repository<PpvEvent>,
  ) { }

  getPpvEvent(eventId: string): Promise<PpvEvent> {
    return this.ppvEventRepo.findOne(eventId);
  }

  createPpvEvent(data: DeepPartial<PpvEvent>): Promise<PpvEvent> {
    return this.ppvEventRepo.save(data);
  }
}
