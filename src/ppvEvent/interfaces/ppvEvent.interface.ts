export interface IPpvEvent {
  id: string,
  name: string,
  categoryId: string,
}