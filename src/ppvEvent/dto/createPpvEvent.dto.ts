import { IsUUID, MinLength } from 'class-validator';

export class CreatePpvEventDto {
  @MinLength(3)
  name: string;

  @IsUUID()
  categoryId: string;
}